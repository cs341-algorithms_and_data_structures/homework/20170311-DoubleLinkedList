/*
 * DlList.cpp
 *
 *  Created on: Mar 11, 2017
 *      Author: axyd
 */

#include "DLList.h"

template <typename V1, typename V2 >
DLList <V1, V2>::DLList()
	: head(nullptr), tail(nullptr) {
	nodeCount= 0;
	
	head= new Node <V1, V2>;
	tail= new Node <V1, V2>;
}

template <typename V1, typename V2 >
DLList <V1, V2>::~DLList() {
	
	cout << "\n\tDestroyed Double Linked List\n\n";
	
}

template <typename V1, typename V2 >
void DLList <V1, V2>::firstNode() {
	cout << "\n#Status: creating new list\n";
	
	/*Link head and tail*/
	tail->prev= head->next;
	head->next= tail->prev;
	tail->next= nullptr;  //end of list
}

template <typename V1, typename V2 >
void DLList <V1, V2>::insertFront(Node <V1, V2>& theNode) {
	if (isEmpty()) {
		head= &theNode;
		firstNode();  //link head and tail
		
	} else {
		Node <V1, V2> *temp= head;  //keep track of first node
		head= &theNode;  //head is now at new node location
		
		/*Relink head with the list*/
		head->next= temp->prev;
		temp->prev= head->next;
		
		/*Free resources*/
		temp= nullptr;
		delete temp;
	}
	++nodeCount;
}

template <typename V1, typename V2 >
void DLList <V1, V2>::insertBack(Node <V1, V2>& theNode) {
	
}

template <typename V1, typename V2 >
void DLList <V1, V2>::printNodes() {
	if (isEmpty()) {
		cout << "\n\t/!\\The `list is empty/!\\\n";
	} else {
		for (unsigned int ctr= 0; ctr < nodeCount; ++ctr) {
			Node <V1, V2> pos;
			pos= head;  //first position
		}
	}
}
