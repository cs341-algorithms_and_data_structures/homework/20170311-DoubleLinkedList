//============================================================================
// Name        : DoublyLinkedList.cpp
// Author      : axyd
// Version     :
// Copyright   : none
// Description : Testing Doubly Linked List
//============================================================================

#include <iostream>
#include "DLList.cpp"
#include "DLList.h"
using namespace std;

int main() {
	
	DLList <int, string> dll;
	
	Node <int, string> *ntemp= new Node <int, string>;
	ntemp->value1= 69;
	ntemp->value2= "fuck";
	
	string choice= "", subChoice= "";
	char quit= false;
	
	do {
		cout << "\t\tMAIN MENU"
			 "\n\t(1) Insert Node"
			 "\n\t(2) Delete Node(s)"
			 "\n\t(3) Show Node(s)"
			 "\n\t(4) Size of List"
			 "\n\t(5) Quit"
			 "\n\n\t>>choice: ";
		
		getline(cin, choice);
		
		switch (choice.at(0)) {  //interpret first character
			case '1':

				break;
				
			case '2':
				cout << "\nDELETE:"
					 "\n\t(1) Front"
					 "\n\t(2) Back"
					 "\n\t(3) At Position"
					 "\n\t(4) Return to Main Menu"
					 "\n\n>>choice: ";
				getline(cin, subChoice);
				
				/*Input Validation*/
				while (subChoice.at(0) < '1' || subChoice.at(0) > '4') {
					cout << "\nchoice: ";
					getline(cin, subChoice);
				}
				
				break;
				
			case '3':

				cout << "\nSHOW: "
					 "\n\t(1) Head"
					 "\n\t(2) Tail"
					 "\n\t(3) At Position"
					 "\n\t(4) All"
					 "\n\t(5) Return to Main Menu"
					 "\n\n>>choice:";
				getline(cin, subChoice);
				
				/*Input Validation*/
				while (subChoice.at(0) < '1' || subChoice.at(0) > '5') {
					cout << "\nchoice: ";
					getline(cin, subChoice);
				}
				
				if (subChoice.at(0) == 1) dll.insertFront(*ntemp);
				
				break;
				
			case '4':
				cout << dll.size();

				break;
				
			case '5':
				quit= true;
				break;
				
			default:
				cout << "\n\t/!\\Invalid Selection/!\\";
				
				/*Input Validation*/
				while (choice.at(0) < '1' || choice.at(0) > '5') {
					cout << "\nchoice: ";
					getline(cin, choice);
				}
				
				if (choice.at(0) == 5) quit= true;  //exit program
				
				break;
		}
	} while (quit == false);
	
	system("CLS");
	cout << "\n\n\n\n\n\t\tGOODBYE!\n\n\n\n\n";
	system("PAUSE");
	
	return 0;
}
