/*
 * DlList.h
 *
 *  Created on: Mar 11, 2017
 *      Author: axyd
 */

#ifndef DLLIST_H_
#define DLLIST_H_

#include <iostream>
using namespace std;

/*Forward Declare for being friend*/
template <typename V1, typename V2 > class DLList;

template <typename V1, typename V2 >
struct Node {
		/*Optional Parameters*/
		Node()
			: prev(nullptr), next(nullptr) {
			
		}
		
//		Node(V1 &v1= 0, V2 &v2= 0, Node *pr= nullptr, Node *nx= nullptr)
//			: value1(v1), value2(v2), prev(pr), next(nx) {
//			
//		}
		
		/*DATA STORAGE*/
		V1 value1;
		V2 value2;

		/*NODE POINTERS*/
		Node *prev;
		Node *next;

//		friend class DLList <V1, V2> ;
		friend V1;
		friend V2;
};

template <typename V1, typename V2 >
class DLList {
	public:
		DLList();
		~DLList();

		bool isEmpty() const {
			return !(head->next == nullptr);
		}
		bool size() const {
			return nodeCount;
		}
		void firstNode();
		void insertFront(Node <V1, V2>&);
		void insertBack(Node <V1, V2>&);
		void printNodes();

	private:
		Node <V1, V2> *head;
		Node <V1, V2> *tail;
		int nodeCount;
};

#endif /* DLLIST_H_ */
